# Getch

Rust crate to get a single key press at the terminal (i.e. without pressing enter), both on Windows and Linux.

## Contributing

We welcome contributions.

By contributing, you agree to license all your contributions under the Apache 2.0 license.

Moreover, the main platform for contributing is [the Nest](//nest.pijul.com/pijul_org/getch), which is still at an experimental stage. Therefore, even though we do our best to avoid it, our repository might be reset, causing the patches of all contributors to be merged.
