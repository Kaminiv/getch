#[cfg(not(windows))]
extern crate termios;
#[cfg(not(windows))]
use std::io::Read;
#[cfg(windows)]
pub struct Getch {}

#[cfg(not(windows))]
pub enum Getch {
    TTY(termios::Termios),
    NOTTY,
}

extern crate libc;

#[cfg(windows)]
use libc::c_int;
#[cfg(windows)]
extern "C" {
    fn _getch() -> c_int;
}

#[cfg(not(windows))]
use termios::{tcsetattr, ICANON, ECHO};

impl Getch {
    #[cfg(windows)]
    pub fn new() -> Result<Getch, std::io::Error> {
        Ok(Getch {})
    }
    #[cfg(not(windows))]
    pub fn new() -> Result<Getch, std::io::Error> {
        match termios::Termios::from_fd(0) {
            Ok(mut termios) => {
                let c_lflag = termios.c_lflag;
                termios.c_lflag &= !(ICANON | ECHO);

                match tcsetattr(0, termios::TCSADRAIN, &termios) {
                    Ok(()) => (),
                    Err(_) => (),
                }
                termios.c_lflag = c_lflag;
                Ok(Getch::TTY(termios))
            }
            Err(e) => {
                if e.raw_os_error() == Some(libc::ENOTTY) {
                    Ok(Getch::NOTTY)
                } else {
                    Err(e)
                }
            }
        }
    }

    #[cfg(windows)]
    pub fn getch(&self) -> Result<u8, std::io::Error> {
        loop {
            unsafe {
                let k = _getch();
                if k == 0 {
                    // Ignore next input.
                    _getch();
                } else {
                    return Ok(k as u8);
                }
            }
        }
    }

    #[cfg(not(windows))]
    pub fn getch(&self) -> Result<u8, std::io::Error> {
        let mut r: [u8; 1] = [0];
        let mut stdin = std::io::stdin();
        loop {
            if try!(stdin.read(&mut r[..])) == 0 {
                return Ok(0);
            } else {
                if r[0] == 27 {
                    if try!(stdin.read(&mut r[..])) == 0 {
                        return Ok(0);
                    } else {
                        if r[0] == 91 {
                            if try!(stdin.read(&mut r[..])) == 0 {
                                return Ok(0);
                            }
                        }
                    }
                } else {
                    // TODO: accept utf-8
                    return Ok(r[0]);
                }
            }
        }
    }
}

impl Drop for Getch {
    #[cfg(not(windows))]
    fn drop(&mut self) {
        match *self {
            Getch::TTY(ref mut t) => tcsetattr(0, termios::TCSADRAIN, &t).unwrap_or(()),
            Getch::NOTTY => (),
        }
    }

    #[cfg(windows)]
    fn drop(&mut self) {}
}
